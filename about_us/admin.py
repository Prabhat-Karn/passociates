from django.contrib import admin

from about_us.models import AboutUs
# Register your models here.

class AboutUsAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(AboutUs, AboutUsAdmin)
