from django.db import models
from django.conf import settings
# Create your models here.
class AboutUs(models.Model):
    title = models.CharField(max_length=50)
    body = models.TextField()
    awards_received = models.PositiveIntegerField(blank = True, null = True)
    no_of_staff = models.PositiveIntegerField(blank = True, null = True)
    projects_completed = models.PositiveIntegerField(blank = True, null = True)
    happy_client = models.PositiveIntegerField(blank = True, null = True)

    def __str__(self):
        return self.title

# class AboutUsGallery(models.Model):
#     about_us = models.ForeignKey(AboutUs, default = None, related_name = 'about' , on_delete=models.CASCADE)
#     images = models.FileField(upload_to= 'images/')

#     def __str__(self):
#         return self.about_us.title

#     @property
#     def image_url(self):
#         return "{0}{1}".format(settings.MEDIA_URL, self.images.url)
    