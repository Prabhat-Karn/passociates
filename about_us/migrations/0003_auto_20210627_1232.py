# Generated by Django 3.1.2 on 2021-06-27 12:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0002_auto_20210626_1403'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aboutus',
            name='image',
        ),
        migrations.AddField(
            model_name='aboutus',
            name='awards_received',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aboutus',
            name='happy_client',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aboutus',
            name='no_of_staff',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aboutus',
            name='projects_completed',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.DeleteModel(
            name='AboutUsGallery',
        ),
    ]
