from rest_framework import serializers
from about_us.models import AboutUs

class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUs
        fields = ['title','body','awards_received','no_of_staff','projects_completed','happy_client']
