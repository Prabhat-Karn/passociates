from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from about_us.models import AboutUs
from about_us.api.serializers import AboutUsSerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class AboutUsView(APIView):

    def get_object(self, request):

        try:
            return AboutUs.objects.all()
        except AboutUs.DoesNotExist:
            raise HTTP_404_BAD_REQUEST

    def get(self, request, format = None):

        about_us =  self.get_object(request)
        serializer = AboutUsSerializer(about_us, many = True)
        return Response(serializer.data)