from django.urls import path
from about_us.api import views
# from about_us.views import Index

app_name = 'about_us'


urlpatterns = [
    path('about_us/', views.AboutUsView.as_view(), name = 'about us'),
    # path('', Index.as_view(), name = 'index page')
]