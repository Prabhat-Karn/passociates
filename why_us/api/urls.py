from django.urls import path
from why_us.api import views

app_name = 'why_us'


urlpatterns = [
    path('why_us/', views.WhyUsView.as_view(), name = 'why us'),
]