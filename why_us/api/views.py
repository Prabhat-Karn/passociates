from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from why_us.models import WhyUs
from why_us.api.serializers import WhyUsSerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class WhyUsView(APIView):

    def get_object(self, request):

        try:
            return WhyUs.objects.all()
        except WhyUs.DoesNotExist:
            raise HTTP_404_BAD_REQUEST

    def get(self, request, format = None):

        why_us =  self.get_object(request)
        serializer = WhyUsSerializer(why_us, many = True)
        return Response(serializer.data)