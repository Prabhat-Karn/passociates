from rest_framework import serializers
from why_us.models import WhyUs
class WhyUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = WhyUs
        fields = ['title','sub_title','body','image']