from django.contrib import admin
from why_us.models import WhyUs
# Register your models here.
class WhyUsAdmin(admin.ModelAdmin):
    list_display = ('id','title','sub_title')
    search_fields = ('title',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(WhyUs, WhyUsAdmin)