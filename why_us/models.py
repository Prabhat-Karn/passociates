from django.db import models

# Create your models here.
class WhyUs(models.Model):
    title = models.CharField(max_length=50)
    sub_title = models.CharField(max_length=50, blank = True, null = True)
    body = models.TextField()
    image = models.FileField(blank = True)

    def __str__(self):
        return self.title