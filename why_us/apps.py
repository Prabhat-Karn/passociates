from django.apps import AppConfig


class WhyUsConfig(AppConfig):
    name = 'why_us'
