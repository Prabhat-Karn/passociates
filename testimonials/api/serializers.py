from rest_framework import serializers
from testimonials.models import Testimonial
class TestimonialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testimonial
        fields = ['sender_name','sender_profession','description','image']