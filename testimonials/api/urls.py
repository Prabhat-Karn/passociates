from django.urls import path
from testimonials.api import views

app_name = 'testimonials'


urlpatterns = [
    path('testimonials/', views.TestimonialView.as_view(), name = 'why us'),
]