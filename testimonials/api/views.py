from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from testimonials.models import Testimonial
from testimonials.api.serializers import TestimonialSerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class TestimonialView(APIView):

    def get_object(self, request):

        try:
            return Testimonial.objects.all()
        except Testimonial.DoesNotExist:
            raise HTTP_404_BAD_REQUEST

    def get(self, request, format = None):

        testimonials =  self.get_object(request)
        serializer = TestimonialSerializer(testimonials, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        serializer = TestimonialSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Testimonial Submitted!'},status = HTTP_200_OK)
        return Response(serializer.errors,status = HTTP_400_BAD_REQUEST)