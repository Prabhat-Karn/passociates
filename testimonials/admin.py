from django.contrib import admin
from testimonials.models import Testimonial
# Register your models here.
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('id','sender_name','sender_profession','description')
    search_fields = ('sender_name',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Testimonial, TestimonialAdmin)