from django.db import models

# Create your models here.
class Testimonial(models.Model):
    sender_name = models.CharField(max_length=50)
    sender_profession = models.CharField(max_length=50, blank = True, null = True)
    description = models.TextField()
    image = models.FileField(blank = True)

    def __str__(self):
        return self.sender_name
    