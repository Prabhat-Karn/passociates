from django.apps import AppConfig


class ArchitectureAndConstructionConfig(AppConfig):
    name = 'architecture_and_construction'
