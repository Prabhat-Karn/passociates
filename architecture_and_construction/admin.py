from django.contrib import admin

from architecture_and_construction.models import Category, SubCategory, Project, ProjectImage
# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category_name')
    search_fields = ('category_name',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category','sub_category')
    search_fields = ('sub_category',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()
    

class ProjectImageAdmin(admin.StackedInline):
    model = ProjectImage

class ProjectAdmin(admin.ModelAdmin):
    inlines = [ProjectImageAdmin]

    raw_id_fields = ('category','sub_category')
        
class ProjectImageAdmin(admin.ModelAdmin):
    pass

admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectImage, ProjectImageAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
