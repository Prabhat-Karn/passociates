from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from architecture_and_construction.models import Project, Category
from architecture_and_construction.api.serializers import ProjectSerializer, CategorySerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class ProjectView(APIView):

    def get_object(self, request):

        try:
            # return Category.objects.all()
            return Category.objects.all()
        except Project.DoesNotExist:
            raise HTTP_404_BAD_REQUEST

    def get(self, request, format = None):

        project =  self.get_object(request)
        serializer = CategorySerializer(project, many = True)
        return Response(serializer.data)