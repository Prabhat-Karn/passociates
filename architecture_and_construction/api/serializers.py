from rest_framework import serializers
from architecture_and_construction.models import Project, Category

# class ProjectSerializer(serializers.ModelSerializer):
#     project_images = serializers.SlugRelatedField(
#         source = 'project_details',
#         slug_field = 'image_url',
#         many = True,
#         read_only = True
#     )
#     category = serializers.CharField(source = 'category.category_name', read_only = True)
#     sub_category = serializers.CharField(source = 'sub_category.subcategory', read_only = True)

#     class Meta:
#         model = Project
#         fields = ['category','sub_category','project_name','description','project_images']

class ProjectSerializer(serializers.ModelSerializer):
    project_images = serializers.SlugRelatedField(
        source = 'image_list',
        slug_field = 'image_url',
        many = True,
        read_only = True
    )
    class Meta:
        model = Project
        fields = ['id','project_name','description','project_images']

class CategorySerializer(serializers.ModelSerializer):
    project_details = ProjectSerializer(many = True, read_only = True)
    class Meta:
        model = Category
        fields = ['category_name','project_details']