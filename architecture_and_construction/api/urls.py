from django.urls import path
from architecture_and_construction.api import views

app_name = 'architecture_and_construction'


urlpatterns = [
    path('architecture_and_construction/', views.ProjectView.as_view(), name = 'architecture and construction'),
]