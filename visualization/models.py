from django.db import models
from django.conf import settings
# Create your models here.
class VCategory(models.Model):
    category_name = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name

class VSubCategory(models.Model):
    category = models.ForeignKey(VCategory, on_delete=models.CASCADE)
    sub_category = models.CharField(max_length=100)
    
    def __str__(self):
        return self.sub_category

class VProject(models.Model):
    category = models.ForeignKey(VCategory,related_name = 'v_project_details', on_delete=models.CASCADE)
    sub_category = models.ForeignKey(VSubCategory, on_delete=models.CASCADE, blank = True, null = True)
    project_name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.FileField(blank = True)

    def __str__(self):
        return self.project_name

class VProjectImage(models.Model):
    project = models.ForeignKey(VProject, default = None, related_name = 'v_image_list', on_delete=models.CASCADE)
    images = models.FileField(upload_to= 'images/')

    def __str__(self):
        return self.project.project_name

    @property
    def image_url(self):
        # return "{0}{1}".format(settings.MEDIA_URL, self.images.url)
        return "{0}{1}".format(settings.MEDIA_URL, self.images)