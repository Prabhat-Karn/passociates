from rest_framework import serializers
from visualization.models import VCategory, VProject
# class VProjectSerializer(serializers.ModelSerializer):
#     v_project_images = serializers.SlugRelatedField(
#         source = 'v_project_details',
#         slug_field = 'image_url',
#         many = True,
#         read_only = True
#     )
#     category = serializers.CharField(source = 'category.category_name', read_only = True)
#     sub_category = serializers.CharField(source = 'sub_category.subcategory', read_only = True)

#     class Meta:
#         model = VProject
#         fields = ['category','sub_category','project_name','description','v_project_images']



class VProjectSerializer(serializers.ModelSerializer):
    project_images = serializers.SlugRelatedField(
        source = 'v_image_list',
        slug_field = 'image_url',
        many = True,
        read_only = True
    )
    class Meta:
        model = VProject
        fields = ['id','project_name','description','project_images']

class VCategorySerializer(serializers.ModelSerializer):
    v_project_details = VProjectSerializer(many = True, read_only = True)
    class Meta:
        model = VCategory
        fields = ['category_name','v_project_details']