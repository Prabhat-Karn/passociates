from django.urls import path
from visualization.api import views

app_name = 'visualization'


urlpatterns = [
    path('visualization/', views.VProjectView.as_view(), name = 'visualization'),
]