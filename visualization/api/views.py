from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from visualization.models import VCategory
from visualization.api.serializers import VCategorySerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class VProjectView(APIView):

    def get_object(self, request):

        try:
            # return Category.objects.all()
            return VCategory.objects.all()
        except VCategory.DoesNotExist:
            raise HTTP_404_BAD_REQUEST

    def get(self, request, format = None):

        project =  self.get_object(request)
        serializer = VCategorySerializer(project, many = True)
        return Response(serializer.data)