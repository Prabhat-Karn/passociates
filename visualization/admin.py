from django.contrib import admin

from visualization.models import VCategory, VSubCategory, VProject, VProjectImage
# Register your models here.

class VCategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category_name')
    search_fields = ('category_name',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class VSubCategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category','sub_category')
    search_fields = ('sub_category',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()
    

class VProjectImageAdmin(admin.StackedInline):
    model = VProjectImage

class VProjectAdmin(admin.ModelAdmin):
    inlines = [VProjectImageAdmin]

    raw_id_fields = ('category','sub_category')
        
class VProjectImageAdmin(admin.ModelAdmin):
    pass

admin.site.register(VProject, VProjectAdmin)
admin.site.register(VProjectImage, VProjectImageAdmin)
admin.site.register(VCategory, VCategoryAdmin)
admin.site.register(VSubCategory, VSubCategoryAdmin)
