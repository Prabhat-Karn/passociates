from django.contrib import admin
from contact_us.models import Contact
# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display = ('name','email','subject','message')
    search_fields = ('name',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Contact, ContactAdmin)