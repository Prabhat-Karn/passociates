from django.urls import path
from contact_us.api import views

app_name = 'contact_us'


urlpatterns = [
    path('contact_us/', views.ContactView.as_view(), name = 'contact us'),
]