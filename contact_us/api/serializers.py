from rest_framework import serializers
from contact_us.models import Contact
class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ['name','email','subject','message']