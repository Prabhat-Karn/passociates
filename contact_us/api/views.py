from rest_framework.views import APIView
from rest_framework.response import Response
from django.views import View
from django.shortcuts import render, redirect
from contact_us.models import Contact
from contact_us.api.serializers import ContactSerializer
from rest_framework.status import(
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

class ContactView(APIView):

    def post(self, request, format = None):
        serializer = ContactSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Message Sent Successfully!'},status = HTTP_200_OK)
        return Response(serializer.errors, status = HTTP_400_BAD_REQUEST)