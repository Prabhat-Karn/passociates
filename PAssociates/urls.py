"""PAssociates URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('about_us.api.urls')),
    path('',include('architecture_and_construction.api.urls')),
    path('',include('visualization.api.urls')),
    path('',include('gallery.api.urls')),
    path('',include('why_us.api.urls')),
    path('',include('testimonials.api.urls')),
    path('',include('contact_us.api.urls'))

] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

admin.site.site_header = "PAssociates Admin"
admin.site.site_title = "PAssociates Admin Portal"
admin.site.index_title = "Welcome to PAssociates"