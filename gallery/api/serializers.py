from rest_framework import serializers
from gallery.models import Gallery, GalleryImage

class GalleryImageSerializer(serializers.ModelSerializer):
    gallery_images = serializers.SlugRelatedField(
        source = 'gallery',
        slug_field = 'image_url',
        many = True,
        read_only = True
    )
    # category =  serializers.CharField(source = 'gallery.category', read_only = True)
    # sub_category =  serializers.CharField(source = 'gallery.sub_category', read_only = True)

    class Meta:
        model = Gallery
        fields = ['category','sub_category','gallery_images']