from django.urls import path
from gallery.api import views

app_name = 'gallery'


urlpatterns = [
    path('gallery/', views.GalleryView.as_view(), name = 'about us'),
]