from django.db import models
from django.conf import settings
# Create your models here.
class Gallery(models.Model):
    category = models.CharField(max_length=50)
    sub_category = models.CharField(max_length=50, blank = True, null = True)

    def __str__(self):
        return self.category

class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery, default = None, related_name = 'gallery' , on_delete=models.CASCADE)
    images = models.FileField(upload_to= 'images/')

    def __str__(self):
        return self.gallery.category

    @property
    def image_url(self):
        return "{0}{1}".format(settings.MEDIA_URL, self.images.url)
    