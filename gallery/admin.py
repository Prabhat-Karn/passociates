from django.contrib import admin

from gallery.models import Gallery, GalleryImage
# Register your models here.

class GalleryImageAdmin(admin.StackedInline):
    model = GalleryImage

class GalleryAdmin(admin.ModelAdmin):
    inlines = [GalleryImageAdmin]
        
class GalleryImageAdmin(admin.ModelAdmin):
    pass

admin.site.register(Gallery, GalleryAdmin)
admin.site.register(GalleryImage, GalleryImageAdmin)